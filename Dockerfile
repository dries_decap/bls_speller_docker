FROM ddecap/blsspeller-base

USER root

ENV BLSSPELLER_V=1.1
COPY blsspeller /usr/local/blsspeller/bin/
COPY bootstrap.sh /usr/local/blsspeller/

RUN cd /usr/local/blsspeller/bin &&git clone https://dries_decap@bitbucket.org/dries_decap/suffixtree-motif-speller.git && \
    cd suffixtree-motif-speller/motifIterator && git clone https://github.com/google/marl.git && \
    mkdir build && cd build && cmake ../ && make && ln -s /usr/local/blsspeller/bin/suffixtree-motif-speller/motifIterator/build/motifIterator /usr/local/blsspeller/bin/motifIterator && \
    cd /usr/local/blsspeller/lib && wget https://bitbucket.org/dries_decap/bls-speller-spark/downloads/bls-speller-assembly-${BLSSPELLER_V}.jar && cd /usr/local/blsspeller && \
    useradd -M -d /usr/local/blsspeller/ -s /bin/bash blsspeller && mkdir /usr/local/blsspeller/.ssh/ && \
    chown -R blsspeller:blsspeller /usr/local/blsspeller/ && echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config
USER blsspeller
WORKDIR /usr/local/blsspeller/
ENTRYPOINT ["/usr/local/blsspeller/bootstrap.sh"]
CMD ["-b"]
