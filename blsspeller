#!/bin/bash

echo "BlsSpeller version ${BLSSPELLER_V}"

help="blsspeller 1|2|full input blssthresholds minlen,maxlen alphabet degen alignmentBasedBoolean [options] :\n\
 -h|--help\tshow help\n\
 -t|--threads\tnumber of CPUs to use\n\
 --family_cutoff int\tfamily count cutoff\n\
 --conf_cutoff int\tconfidence cutoff in percent\n\
 --fastas string\tfasta file or folder of fasta files to create bed files; promotor region range is [start, stop[ \n\
 --gene_start_pos_exclusive\tgene promotor region range start is exclusive\n\
 --gene_stop_pos_inclusive\tgene promotor region range stop is inclusive\n\
 --strandaware\tgene promotor region locations are strand aware\n\
 --AB\talignment based\n\
 --interactive\tinteractive mode to determine cutoffs in step 2"

# set global variables
UUID=$(uuidgen)
outdir=$(pwd)/output_${UUID}
EXECUTORS=${RESOURCE_CPU}
EXECUTOR_MEMORY="4000m"
EXECUTOR_OVERHEAD="2000"
PARTITIONS=$((EXECUTORS*10))

function step1() {
  local input=$1; shift;
  local blsthresholds=$1; shift;
  local length=$1; shift;
  local alphabet=$1; shift;
  local degen=$1; shift;
  local threads=$1; shift;
  local AlignmentBased=$1; shift;

  alingmenttype=""
  if [[ ${AlignmentBased} == "true" ]]; then
    alingmenttype="--AB"
  fi

  local alphabetoption=""
  if [[ $alphabet == "3" ]]; then
    alphabetoption="--fullIupac";
  elif [[ $alphabet == "2" ]]; then
    alphabetoption="--twofoldAndN";
  fi
  mkdir -p ${outdir}
  iteratoroutput=${outdir}/l${length/,/-}_a${alphabet}_d${degen}.parquet
  local logfile=${outdir}/l${length/,/-}_a${alphabet}_d${degen}.log
  echo "iterating motifs..."
  cat $input/* | motifIterator discovery - --bls ${blsthresholds} ${alingmenttype} --lengthrange ${length} --degen ${degen} ${alphabetoption} --count --threads ${threads} \
    --parquet ${iteratoroutput} 2>${logfile}
  returnval=$?
  if [ $returnval -ne 0 ]; then
   echo "BLS Speller iterating motifs FAILED; Log is at ${logfile};"
   return -2;
  fi
  tail -n 1 ${logfile}
  motifoutput="${iteratoroutput/.parquet/.motifs}"
  echo "calculating confidence scores..."
  spark-submit --master yarn --conf spark.ui.showConsoleProgress=true \
          --executor-memory ${EXECUTOR_MEMORY} --num-executors ${EXECUTORS} --executor-cores 1 \
          --conf spark.task.cpus=1 --conf spark.yarn.stagingDir=/tmp/sparkStaging \
          --jars ${HOME}lib/bls-speller-assembly-${BLSSPELLER_V}.jar \
          --class be.ugent.intec.ddecap.BlsSpeller ${HOME}lib/bls-speller-assembly-${BLSSPELLER_V}.jar \
          getMotifs --input ${iteratoroutput} --output ${motifoutput} \
          --bindir ${HOME}bin --partitions ${PARTITIONS} ${alingmenttype} \
          --alphabet ${alphabet} --degen ${degen} --min_len $(echo ${length} | awk -F"," '{print $1}') --max_len $(echo ${length} | awk -F"," '{print $2}') \
          --bls_thresholds ${blsthresholds} --parquet | tee -a ${logfile}
  if [[ ! -f "${motifoutput}/_SUCCESS" ]]; then
   echo "BLS Speller calculating confidence score FAILED; see http://${HOSTNAME}:8088 for more information"
   return -1;
  fi
  # rm -r ${iteratoroutput}
  return 0
}

function step2() {
  local input=$1; shift;
  local fastas=$1; shift;
  local blsthresholds=$1; shift;
  local length=$1; shift;
  local alphabet=$1; shift;
  local degen=$1; shift;
  local family_cutoff=$1; shift;
  local conf_percent=$1; shift
  local gene_start_pos_inclusive=$1; shift;
  local gene_stop_pos_inclusive=$1; shift;
  local AlignmentBased=$1; shift;
  local strandaware=$1; shift;
  local interactive=$1; shift;

  if [[ "${conf_percent}" == *"."* ]]; then
    conf_fraction=${conf_percent}
  else
    if [[ "${conf_percent}" == "100" ]]; then
    	conf_fraction="1"
    else
    	conf_fraction="0.${conf_percent}"
    fi
  fi
  alingmenttype=""
  if [[ ${AlignmentBased} == "true" ]]; then
    alingmenttype="--AB"
  fi
  gene_strand_aware=""
  if [[ ${strandaware} == "true" ]]; then
    gene_strand_aware="--gene_strand_aware"
  fi

  gene_pos_inclusiveness=""
  if [[ ${gene_start_pos_inclusive} == "false" ]]; then
  	gene_pos_inclusiveness="$gene_pos_inclusiveness --gene_start_exclusive"
  fi
  if [[ ${gene_stop_pos_inclusive} == "true" ]]; then
  	gene_pos_inclusiveness="$gene_pos_inclusiveness --gene_stop_inclusive"
  fi
  gene_awareness=""
  bedoutput="${motifoutput/.motifs/_c"$conf_percent".bed}"
  interactive_option=""
  if [[ ${interactive} == "true" ]]; then
    interactive_option="--interactive"
    bedoutput="${motifoutput/.motifs/_filtered.bed}"
  fi
  local logfile=${outdir}/l${length/,/-}_a${alphabet}_d${degen}.log
  echo "filtering motifs and creating bed files..."
  spark-submit --master yarn --conf spark.ui.showConsoleProgress=true \
          --executor-memory ${EXECUTOR_MEMORY} --conf spark.executor.memoryOverhead=${EXECUTOR_OVERHEAD} \
          --num-executors ${EXECUTORS} --executor-cores 1 --conf spark.task.cpus=1 \
          --conf spark.yarn.stagingDir=/tmp/sparkStaging \
          --jars ${HOME}lib/bls-speller-assembly-${BLSSPELLER_V}.jar \
          --class be.ugent.intec.ddecap.BlsSpeller ${HOME}lib/bls-speller-assembly-${BLSSPELLER_V}.jar \
          locateMotifs --fasta ${fastas} --bindir ${HOME}bin \
          --partitions ${PARTITIONS} --degen ${degen} --max_len $(echo ${length} | awk -F"," '{print $2}') \
          --bls_thresholds ${blsthresholds} --fam_cutoff ${family_cutoff} \
          ${gene_pos_inclusiveness} ${gene_strand_aware} ${alingmenttype} \
          --motifs ${motifoutput} \
          --input ${input} \
          --output ${bedoutput} \
          --conf_cutoff ${conf_fraction} \
          --persist_level disk ${interactive_option} | tee -a ${logfile}
  if [[ ! -f "${bedoutput}/_SUCCESS" ]]; then
   echo "BLS Speller filtering & locating motifs FAILED; see http://${HOSTNAME}:8088 for more information"
   return -1;
  fi
  return 0
}

if [[ $# -lt 7 ]]; then
  echo "exiting; not enough arguments provided;"
  echo -e $help
  exit -3;
fi
# required arguments for both steps:
steps=$1; shift;
input=$1; shift;
blsthresholds=$1; shift;
length=$1; shift;
alphabet=$1; shift;
degen=$1; shift;
alignmentBased=$1; shift;

# parse other arguments
while [[ $# -gt 0 ]]
do
  key="$1"
  case $key in
      -t|--threads)
        threads="$2"
        shift # past argument
        shift # past value
        ;;
      --family_cutoff)
        family_cutoff="$2"
        shift # past argument
        shift # past value
        ;;
      --conf_cutoff)
        conf_cutoff="$2"
        shift # past argument
        shift # past value
        ;;
      --fastas)
        fastas="$2"
        shift # past argument
        shift # past value
        ;;
      --gene_start_pos_exclusive)
        gene_start_pos_inclusive="false"
        shift # past argument
        ;;
      --gene_stop_pos_inclusive)
        gene_stop_pos_inclusive="true"
        shift # past argument
        ;;
      --AB)
        alignmentBased="true"
        shift # past argument
        ;;
      -h|--help)
        echo -e $help
        shift #past argument
        exit; # exit command
        ;;
      --strandaware)
        strandaware="true"
        shift # past argument
        ;;
      --interactive)
        interactive="true"
        shift # past argument
        ;;
      *)    # unknown option
        echo "unknown option $1"
        shift # past argument
        ;;
  esac
done

if [[ $steps == "1" ]]; then
  run_step_1="true";
elif [[ $steps == "2" ]]; then
  run_step_2="true";
  motifoutput=${input}
elif [[ $steps == "full" ]]; then
  run_step_1="true";
  run_step_2="true";
fi

echo "UUID for this run is ${UUID}"

# run step 1 or 2 or both
if [[ $run_step_1 == "true" ]]; then
  # REQUIRED
  # local input=$1; shift;
  # local blsthresholds=$1; shift;
  # local length=$1; shift;
  # local alphabet=$1; shift;
  # local degen=$1; shift;
  # OPTIONAL
  # local threads=$1; shift;
  threads=${threads:-1} # default 1
  # local AlignmentBased=$1; shift;
  alignmentBased=${alignmentBased:-false}

  step1 ${input} ${blsthresholds} ${length} ${alphabet} ${degen} ${threads} ${alignmentBased}
  returnval=$?
  if [ $returnval -ne 0 ]; then
   exit -1;
  fi
fi
if [[ $run_step_2 == "true" ]]; then
  # REQUIRED
  # local input=$1; shift;
  # local fastas=$1; shift;
  if [[ -z ${fastas} ]]; then
    echo "no fasta files given for step 2"
    exit -2
  fi
  # local blsthresholds=$1; shift;
  # local length=$1; shift;
  # local alphabet=$1; shift;
  # local degen=$1; shift;
  # local family_cutoff=$1; shift;
  family_cutoff=${family_cutoff:-1}
  # local conf_percent=$1; shift
  if [[ -z ${conf_cutoff} ]]; then
    echo "no confidence cutoff given for step 2"
    exit -2
  fi
  # OPTIONAL
  # local gene_start_pos_inclusive=$1; shift;
  gene_start_pos_inclusive=${gene_start_pos_inclusive:-true}
  # local gene_stop_pos_inclusive=$1; shift;
  gene_stop_pos_inclusive=${gene_stop_pos_inclusive:-false}
  # local AlignmentBased=$1; shift;
  alignmentBased=${alignmentBased:-false}
  # local strandaware=$1; shift;
  strandaware=${strandaware:-false}
  interactive=${interactive:-false}
  step2 ${input} ${fastas} ${blsthresholds} ${length} ${alphabet} ${degen} ${family_cutoff} ${conf_cutoff} ${gene_start_pos_inclusive} ${gene_stop_pos_inclusive} ${alignmentBased} ${strandaware} ${interactive}
  returnval=$?
  if [ $returnval -ne 0 ]; then
   exit -3;
  fi
fi

echo "output can be found in ./output_${UUID}:"
ls -lh ./output_${UUID}
