#!/bin/bash

help="options:\n\
 -h|--help\tshow help\n\
 -b|--bash\tstart an interactive bash shell\n\
 -m|--mem\tsets the maximum usable memory for spark [default is all]\n\
 -c|--cpus\tsets the maximum usable CPU cores for spark [default is all]\n\
 -r|--run\truns the speller\n\
 \titerate\titerate motifs\n\
 \t\t--input\tinput cluster files\n\
 \tlocate\tfilter & locate motifs\n\
 \t\t--input\tinput cluster files\n\
 \tpreprocess\tpreprocess the promotor sequences\n\
 \t\t--fasta\tfolder of fasta files\n\
 \t\t--newick\tfile with newick tree\n\
 \t\t--orthology\tfile with orothology\n\
 \t\t--output\tfolder for output"

while [[ $# -gt 0 ]]
do
  key="$1"
  case $key in
      -m|--memory)
        RESOURCE_MEMORY="$2" # in GB
        RESOURCE_MEMORY=$((RESOURCE_MEMORY*1024)) # in MB
        shift # past argument
        shift # past value
        ;;
      -c|--cpus)
        RESOURCE_CPU="$2"
        shift # past argument
        shift # past value
        ;;
      -h|--help)
        echo -e $help
        shift #past argument
        exit; # exit command
        ;;
      -b|--bash)
        cmd="/bin/bash"
        BASH="true"
        shift # past argument
        ;;
      -r|--run)
        shift # past argument
        cmd="/usr/local/blsspeller/blsspeller.sh $*"
        break
        # unset "$@"
        ;;
      *)    # unknown option
        echo "unknown option $1"
        shift # past argument
        ;;
  esac
done


## configure HADOOP
if [[ -z $UUID ]]; then
  UUID=$(uuidgen)
fi
HOSTNAME=$(hostname -i)
if [[ -z $DEFAULT_FS ]]; then
  if [[ -z $USE_HDFS ]]; then
    DEFAULT_FS="hdfs://$HOSTNAME:9000"
  else
    DEFAULT_FS="file:///"
  fi
fi
if [[ -z $WORKDIR ]]; then WORKDIR="/tmp/workdir_${UUID}"; fi
mkdir -p "${WORKDIR}"
if [[ -z $DFS_NAMENODE ]]; then
  DFS_NAMENODE="${WORKDIR}/nameNode"
fi
if [[ -z $DFS_DATANODE ]]; then
  DFS_DATANODE="${WORKDIR}/dataNode"
fi
if [[ -z $LOG_DIRECTORY ]]; then
  LOG_DIRECTORY="/tmp/logs_${UUID}"
fi
if [[ -z $LOCAL_DIRS ]]; then
  LOCAL_DIRS="${WORKDIR}/nm-local-dir"
fi
if [[ -z $DFS_REPLICATION ]]; then
  DFS_REPLICATION=1
fi
if [[ -z $RESOURCE_MEMORY ]]; then
  MEM_KB=$(grep MemTotal /proc/meminfo | awk '{print $2}') # in KB
  RESOURCE_MEMORY=$((MEM_KB/1024)) # in MB
fi
if [[ -z $MAXIMUM_MEMORY ]]; then
  MAXIMUM_MEMORY=$RESOURCE_MEMORY
fi
if [[ -z $MINIMUM_MEMORY ]]; then
  MINIMUM_MEMORY=1024
fi
if [[ -z $RESOURCE_CPU ]]; then
  RESOURCE_CPU=$(grep -c processor /proc/cpuinfo) # get cores instead of threads!
fi
if [[ -z $MAXIMUM_CPU ]]; then
  MAXIMUM_CPU=$RESOURCE_CPU
fi
if [[ -z $JAVA_HOME ]]; then
  JAVA_HOME=$(readlink -f $(which java) | sed "s/\/bin\/java//g")
fi
if [[ -z $SPARK_SHUFFLE_JAR ]]; then
  SPARK_SHUFFLE_JAR=$(ls $SPARK_HOME/yarn/spark*-yarn-shuffle.jar)
fi

HADOOP_CONF_DIR=${HADOOP_CONF_DIR:=/usr/local/hadoop/etc/hadoop}
# core-site.xml
sed -i "s#DEFAULT_FS#$DEFAULT_FS#g" $HADOOP_CONF_DIR/core-site.xml
# hdfs-site.xml
sed -i "s#DFS_NAMENODE#$DFS_NAMENODE#g" $HADOOP_CONF_DIR/hdfs-site.xml # not changed
sed -i "s#DFS_DATANODE#$DFS_DATANODE#g" $HADOOP_CONF_DIR/hdfs-site.xml # not changed
sed -i "s#DFS_REPLICATION#$DFS_REPLICATION#g" $HADOOP_CONF_DIR/hdfs-site.xml
# yarn-site.xml
sed -i "s#SPARK_SHUFFLE_JAR#$SPARK_SHUFFLE_JAR#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#RESOURCE_MEMORY#$RESOURCE_MEMORY#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#MAXIMUM_MEMORY#$MAXIMUM_MEMORY#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#MINIMUM_MEMORY#$MINIMUM_MEMORY#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#RESOURCE_CPU#$RESOURCE_CPU#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#MAXIMUM_CPU#$MAXIMUM_CPU#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#LOG_DIRECTORY#$LOG_DIRECTORY#g" $HADOOP_CONF_DIR/yarn-site.xml # not changed
sed -i "s#LOCAL_DIRS#$LOCAL_DIRS#g" $HADOOP_CONF_DIR/yarn-site.xml  # not changed
sed -i "s#HOSTNAME#$HOSTNAME#g" $HADOOP_CONF_DIR/yarn-site.xml
sed -i "s#export JAVA_HOME=\${JAVA_HOME}#export JAVA_HOME=$JAVA_HOME#" $HADOOP_CONF_DIR/hadoop-env.sh
# set slaves
echo "$HOSTNAME" > $HADOOP_CONF_DIR/slaves
#sshd
cat << EOF > ${HOME}/.ssh/sshd_config
Port 22
HostKey ${HOME}/.ssh/id_rsa
HostKey ${HOME}/.ssh/id_dsa
AuthorizedKeysFile  ${HOME}/.ssh/authorized_keys
ChallengeResponseAuthentication no
UsePAM yes
Subsystem   sftp    /usr/lib/ssh/sftp-server
PidFile ${HOME}/.ssh/sshd.pid
EOF
ssh-keygen -f ${HOME}/.ssh/id_rsa -N '' -t rsa >/dev/null && \
  ssh-keygen -f ${HOME}/.ssh/id_dsa -N '' -t dsa >/dev/null && \
  cat ${HOME}/.ssh/id_rsa.pub >> ${HOME}/.ssh/authorized_keys && \
  /usr/sbin/sshd -f ${HOME}/.ssh/sshd_config

# export env
export HOSTNAME WORKDIR JAVA_HOME RESOURCE_MEMORY RESOURCE_CPU

# start yarn
"$HADOOP_PREFIX"/sbin/start-yarn.sh

# run command!
cd /data
exec $cmd

if [[ $BASH != "true" ]]; then # CLEANUP IF NOT INTERACTIVE BASH
  # STOP YARN
  "$HADOOP_PREFIX"/sbin/stop-yarn.sh
fi
